#!/usr/bin/env node
import inputParams from './src/input.js'
import objDecoder from "./src/objDecoder.js";
import rc3Encoder from "./src/rc3Encoder.js";
import summary from "./src/summary.js";

// Доступные импортеры
const importers = {
    'obj': objDecoder
}

// Разбираем исходный файл и ищем парсер'
const importStopwatch = new Date().getTime();
const ext = inputParams.input
    .substring(inputParams.input.lastIndexOf('.') + 1)
    .toLowerCase();
let rawMesh = null;
if (importers[ext] && importers[ext].decode) {
    rawMesh = await importers[ext].decode(inputParams.input);
}
if (rawMesh === null) {
    throw 'Неизвестный формат файла: '+inputParams.input;
}
const importTime = new Date().getTime() - importStopwatch;

// Конвертим полученный файл
const exportStopwatch = new Date().getTime();
await rc3Encoder.encode(
    rawMesh,
    inputParams.output,
    inputParams.vertexCompression,
    inputParams.normalsCompression,
    inputParams.colorsCompression,
    inputParams.skipNormals,
    inputParams.skipColors,
    inputParams.skipTexCoords,
    inputParams.skipMaterials
);
const exportTime = new Date().getTime() - exportStopwatch;

// Итоги по экспорту
await summary(inputParams.input, inputParams.output, importTime, exportTime);



