# RC3 Converter
___
Converts .obj encoded model into .rc3 model.

## Instalation
Update node if you haven't done it recently. 

npm i @red-collar/rc3d-converter --save-dev  

## Options
**-i, --input** [string] [required]. Input file  
**-o, --output** [string] Output file  
**-v, --vertex-compress** [number] Vertex compression value. 0 - no compression. 1 - normal compression. 2 - strong compression. Recommended value 1  
**-n, --normal-compress** [number] Normal compression. 0 - no compression. 1 - normal compression. Recommended value 1  
**-c, --color-compress** [number] Color compression. 0 - no compression. 1 - normal compression. Recommended value 1  
**--skip-normals** [boolean] Don't write normals  
**--skip-colors** [boolean] Don't write colors  
**--skip-texcoords** [boolean] Don't write UV coordinates  
**--skip-materials** [boolean] Don't write materials  

## Usage
```console
npx rc3d-convert -i ./input_file.obj
```

```console
npx rc3d-convert -i ./input_file.obj -v 1 -n 1 -c 1 --skip-normals --skip-colors --skip-texcoords --skip-materials
```