console.clear();

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import replaceExt from 'replace-ext';
import * as fs from 'fs/promises';

// Настройки аргументов
const {argv} = yargs(hideBin(process.argv))
    .usage('Usage: rc3d-convert -i <file> -o <out_file>')
    .option('i', {alias: 'input', describe: 'Входной файл', type: 'string', demandOption: true})
    .option('o', {alias: 'output', describe: 'Выходной файл', type: 'string', demandOption: false})
    .option('v', {alias: 'vertex-compress', describe: 'Сжатие вершин (0-2)', type: 'number', demandOption: false})
    .option('n', {alias: 'normal-compress', describe: 'Сжатие нормалей (0-1)', type: 'number', demandOption: false})
    .option('c', {alias: 'color-compress', describe: 'Сжатие цветов (0-1)', type: 'number', demandOption: false})
    .option('skip-normals', {describe: 'Пропустить нормали', type: 'boolean', demandOption: false})
    .option('skip-colors', {describe: 'Пропустить цвета', type: 'boolean', demandOption: false})
    .option('skip-texcoords', {describe: 'Пропустить текстурные координаты', type: 'boolean', demandOption: false})
    .option('skip-materials', {describe: 'Пропустить материалы', type: 'boolean', demandOption: false});

// Если исходника не существует
try {
    await fs.access(argv.i);
} catch (ex) {
    throw 'Исходный файл отсутствует или недоступен для чтения: '+argv.i;
}

// Если выходной файл не указан - просто поменяем имя исходника
const in_name = argv.i;
let out_name = replaceExt(in_name, '.rc3');
if (argv.o) {
    out_name = argv.o;
}

// Выдача входных параметров
export default {
    input: in_name,
    output: out_name,
    vertexCompression: argv.v || 0,
    normalsCompression: argv.n || 0,
    colorsCompression: argv.c || 0,
    skipNormals: !!argv.skipNormals,
    skipColors: !!argv.skipColors,
    skipTexCoords: !!argv.skipTexcoords,
    skipMaterials: !!argv.skipMaterials
}