import {eachLine} from "line-reader";
import * as fs from 'fs/promises';

export default {

    /**
     * Декодинг OBJ-файла
     * @param file
     * @returns {Promise<Object>}
     */
    async decode(file) {

        // Инфо по файлу
        console.log('='.repeat(80));
        console.log('OBJ Decoder v1.1, файл:', file);
        console.log('Чтение файла...');

        // Загрузка самого файла
        const model = await this.readOBJFile(file);
        let surfaces = [];

        // Если у меша есть либы с материалами
        let materials = [];
        try {
            if (model.materials && model.materials.length) {
                console.log('Чтение материалов...');
                const tasks = model.materials.map(mat => {
                    if (mat.lastIndexOf('/') === -1) {
                        mat = file.split('\\').join('/').substring(0, file.split('\\').join('/').lastIndexOf('/') + 1) + mat;
                    }
                    return this.readMTLFile(mat);
                });
                materials = [].concat(...(await Promise.all(tasks)));
                console.log('')
            }
        } catch (e) {
            console.log(e)
        }


        // Сдвиги для общего количества вершин
        let sub = {
            verts: 0,
            normals: 0,
            texCoords: 0,
        }
        for (let surface of model.meshes) {

            // Перевод сюрфейса в общий формат
            console.log('Конвертация сурфейса:', surface.name);
            const outSurf = this.convertSurface(surface, sub, materials);
            if (outSurf !== null) {
                surfaces.push(outSurf);
                console.log('Вершин:', surface.verts.length, '=>', outSurf.verts.length);
                console.log('Индексов:', surface.faces.length, '=>', outSurf.indices.length / 3);
            } else {
                console.log('[!] Сурфейс пуст, пропускаем');
            }

            // Если у сюрфейса есть вершины, их надо плюсануть в лукап
            if (surface.verts) sub.verts += surface.verts.length;
            if (surface.normals) sub.normals += surface.normals.length;
            if (surface.texCoords) sub.texCoords += surface.texCoords.length;
            console.log('');
        }

        // Прочитанные данные
        console.log('Прочитано сурфейсов:', surfaces.length, ', материалов:', materials.length);
        console.log('Внутренних вершин:', sub.verts, ', нормалей', sub.normals, ', текстурных координат', sub.texCoords)
        console.log('');

        // Выдача
        return {
            surfaces,
            materials
        };
    },

    /**
     * Чтение файла
     * @param file
     * @returns {Promise<[]>}
     */
    readOBJFile(file) {
        return new Promise(((resolve, reject) => {

            // Выдаваемые меши
            let meshes = [];
            let materialFiles = [];
            let currentMesh = null;

            // Чтение файла
            eachLine(file, function(line, last) {

                // Срез комментариев
                if (line.indexOf('#') !== -1) {
                    line = line.substr(0, line.indexOf('#'));
                }
                line = line.trim().replace("\t", ' ').replace('  ', ' ');
                if (!line.length) return;

                // Разбивка на токены и параметры
                const fields = line.split(' ').filter(item => item.trim().length > 0);
                const op = fields.shift().toLowerCase();
                switch (op) {

                    // Поднятие MTL-файла
                    case 'mtllib':
                        materialFiles.push(fields[0]);
                        console.log('Файл материалов:', fields[0]);
                        break;

                    // Включение материала
                    case 'usemtl':
                        currentMesh.material =
                            fields[0] !== 'None' ?
                            fields[0] :
                            null;
                        break;

                    // Название объекта
                    case 'o':
                        currentMesh = {
                            name: fields[0],
                            material: null,
                        };
                        meshes.push(currentMesh);
                        console.log('Читаю сабмеш:', currentMesh.name);
                        break;

                    // Вершина - координаты в пространстве
                    // Если у вершины 3 или 4 параметра - это [x, y, z, w]
                    // Если у вершины 6 параметров - это [x, y, z, r, g, b]
                    case 'v':
                        if (!currentMesh.verts) {
                            currentMesh.verts = [];
                        }
                        const vert = {};
                        vert.coords = [
                            parseFloat(fields[0]),
                            parseFloat(fields[1]),
                            parseFloat(fields[2]),
                        ];
                        if (fields.length === 6) {
                            vert.color = [
                                parseFloat(fields[3]),
                                parseFloat(fields[4]),
                                parseFloat(fields[5]),
                            ]
                        } else {
                            vert.color = false;
                        }
                        currentMesh.verts.push(vert);
                        break;

                    // Нормаль - вектор куда смотрит вершина
                    case 'vn':
                        if (!currentMesh.normals) {
                            currentMesh.normals = [];
                        }
                        currentMesh.normals.push([
                            parseFloat(fields[0]),
                            parseFloat(fields[1]),
                            parseFloat(fields[2]),
                        ])
                        break;

                    // Текстурная координата - тут всё просто, это uv
                    case 'vt':
                        if (!currentMesh.texCoords) {
                            currentMesh.texCoords = [];
                        }
                        currentMesh.texCoords.push([
                            parseFloat(fields[0]),
                            parseFloat(fields[1]),
                        ])
                        break;

                    // Фейс
                    case 'f':
                        if (!currentMesh.faces) {
                            currentMesh.faces = [];
                        }
                        currentMesh.faces.push(fields);
                        break;

                    default:
                        // Непонятный тег
                        if (!([
                            's',    // <- включение флетшейдинга
                            'g',    // <- группа треугольников
                            'l'     // <- часть кривой
                        ].includes(op))) {
                            console.log('Неизвестный тег: '+op)
                        }
                        break;
                }

                // Выдача
                if (last) {
                    resolve({
                        meshes: meshes,
                        materials: materialFiles
                    });
                }
            });
        }))
    },

    /**
     * Чтение файла с материалами
     * @param file
     * @returns {Promise<[]>}
     */
    readMTLFile(file) {
        return new Promise(((resolve, reject) => {

            // Выдаваемые меши
            let materials = [];
            let currentMaterial = null;

            const path = function (p) {
                p = p.split('\\').join('/');
                if (p.lastIndexOf('/') !== -1) {
                    p = p.substring(p.lastIndexOf('/') + 1);
                }
                return p;
            }

            // Проверка на наличие файла
            fs.access(file).then(() => {
                console.log('Файл:', file)

                // Чтение файла
                try {
                    eachLine(file, function(line, last) {

                        // Срез комментариев
                        if (line.indexOf('#') !== -1) {
                            line = line.substr(0, line.indexOf('#'));
                        }
                        line = line.trim().split("\t").join(' ');
                        if (!line.length) return;

                        // Разбивка на токены и параметры
                        const fields = line.split(' ').filter(item => item.trim().length > 0);
                        const op = fields.shift().toLowerCase();
                        switch (op) {

                            // Новый материал
                            case 'newmtl':
                                currentMaterial = {
                                    name: fields[0],
                                    ambient: [0, 0, 0],
                                    diffuse: [255, 255, 255],
                                };
                                materials.push(currentMaterial);
                                break;

                            // Амбиентный цвет
                            case 'ka':
                                currentMaterial.ambient = fields.map(color => Math.round(color * 255.0));
                                break;

                            // Диффуз (альбедо)
                            case 'kd':
                                currentMaterial.diffuse = fields.map(color => Math.round(color * 255.0));
                                break;

                            // Эмиссив (излучаемый)
                            case 'ke':
                                currentMaterial.emissive = fields.map(color => Math.round(color * 255.0));
                                break;

                            // Отражаемый цвет
                            case 'ks':
                                currentMaterial.specular = fields.map(color => Math.round(color * 255.0));
                                break;

                            // Вес для спекьюлара (отражаемого)
                            case 'ns':
                                currentMaterial.specular_exponent = parseFloat(fields[0]);
                                break;

                            // Прозрачность
                            case 'd':
                            case 'tr':
                                currentMaterial.opacity = Math.round(parseFloat(fields[0]) * 255.0);
                                if (op === 'tr') {
                                    currentMaterial.opacity = 255 - currentMaterial.opacity;
                                }
                                break;

                            // Текстура дифьюза
                            case 'map_kd':
                                currentMaterial.diffuse_map = path(fields[0]);
                                break;

                            // Карта отражений
                            case 'map_ks':
                                currentMaterial.specular_map = path(fields[0]);
                                break;

                            // Карта эмиссива
                            case 'map_ke':
                                currentMaterial.emissive_map = path(fields[0]);
                                break;

                            // Карта нормалей
                            case 'norm':
                                currentMaterial.normal_map = path(fields[0]);
                                break;

                            // Карта неровностей
                            case 'bump':
                            case 'map_bump':
                                currentMaterial.bump_map = path(fields[0]);
                                break;

                            // Карта прозрачности
                            case 'map_d':
                                currentMaterial.alpha_map = path(fields[0]);
                                break;

                            default:
                                // Непонятный тег
                                if (!([
                                    'ni',           // <- Показатель преломления
                                    'illum',        // <- Режим освещения
                                ].includes(op))) {
                                    console.log('Неизвестный тег: '+op)
                                }
                                break;
                        }

                        // Выдача
                        if (last) {
                            resolve(materials);
                        }
                    });
                } catch (e) {
                    reject(e);
                }
            }).catch(() => {
                resolve([])
            })
        }))
    },

    /**
     * Перевод сюрфейса в человеческий вид
     * @param surf
     * @param subs
     * @param materials
     * @returns {{indices: *[], verts: *[]}|null}
     */
    convertSurface(surf, subs, materials) {

        // Если нет индексов
        if (!surf.faces) {
            return null;
        }

        // Промежуточные массивы
        const name = surf.name;
        let vertLookup = {};
        let verts = [];
        let indices = [];

        // Триангуляция фейсов
        for (let faceArray of surf.faces) {
            let temp = [
                this.createVertex(faceArray[0], surf, verts, vertLookup, subs),
                this.createVertex(faceArray[1], surf, verts, vertLookup, subs),
                this.createVertex(faceArray[2], surf, verts, vertLookup, subs)
            ]
            let lastVert = temp[2];
            if (faceArray.length > 3) {
                for (let i = 3; i < faceArray.length; i++) {
                    let idx = this.createVertex(faceArray[i], surf, verts, vertLookup, subs)
                    temp.push(temp[0]);
                    temp.push(lastVert);
                    temp.push(idx);
                    lastVert = idx;
                }
            }
            indices = indices.concat(temp);
        }

        // Подбор материала
        let material = null;
        if (surf.material !== null) {
            material = materials.findIndex(mat => mat.name === surf.material);
            if (material === -1) material = null;
        }

        // Выдача
        return {
            verts,
            indices,
            name,
            material,
        }
    },

    /**
     * Превращение OBJ-вершины в нормальную
     * Обычно они приходят в формате "позиция/текстура/нормаль"
     * @param index Голый индекс, типа 255/123/32
     * @param surf Данные о сурфейсе
     * @param verts Вершины
     * @param lookup Лукап для кеширования вершин
     * @param subs Отступы
     */
    createVertex(index, surf, verts, lookup, subs) {
        if (!lookup[index]) {
            let parts = index
                .split('/')
                .map(item => item.length ? parseInt(item) - 1 : false)
                .concat([false, false, false])
                .slice(0, 3);
            let out = {
                position: [0, 0, 0],
                normal: false,
                color: false,
                texCoord: false,
                texCoord2: false,
            }

            // Координаты (и цвет)
            if (parts[0] !== false) {
                parts[0] -= subs.verts;
                out.position = surf.verts[parts[0]].coords;
                if (surf.verts[parts[0]].color) {
                    out.position = surf.verts[parts[0]].color;
                }
            }

            // Текстурная координата
            if (parts[1] !== false) {
                parts[1] -= subs.texCoords;
                out.texCoord = surf.texCoords[parts[1]];
            }

            // Нормаль
            if (parts[2] !== false) {
                parts[2] -= subs.normals;
                out.normal = surf.normals[parts[2]];
            }

            // Сохранение вершины
            lookup[index] = verts.length;
            verts.push(out);
        }
        return lookup[index];
    },

}
