import BinaryFile from "binary-file";

export default {

    /**
     * Сжатие модели в RC3-формат
     * @param model Данные из загрузчика
     * @param output Выходной файл
     * @param vertexCompress Сжатие вершин
     * @param normalCompress Сжатие нормалей
     * @param colorCompress Сжатие цвета
     * @param stripNormals Обрезать нормали
     * @param stripColors Обрезать цвет
     * @param stripTexCoords Обрезать текстурные координаты
     * @param stripMaterials Обрезать материалы
     */
    async encode(model, output, vertexCompress, normalCompress, colorCompress, stripNormals, stripColors, stripTexCoords, stripMaterials) {

        console.log('='.repeat(80));
        console.log('RC3 Encoder v1.1, конверт из буффера');
        console.log('Сжатие вершин', vertexCompress, ', нормалей', normalCompress, ', цветов', colorCompress);
        console.log('Пропуск нормалей', stripNormals ? '✔' : '✖', ', цветов', stripColors ? '✔' : '✖', ', текстурных координат', stripTexCoords ? '✔' : '✖');
        console.log('');

        // Метод для проверки строки
        const m = function (item) {
            return (typeof item === 'string') && item.length > 0;
        }

        // Файловый поток
        const f = new BinaryFile(output, 'w', true);
        await f.open();

        // Метод для записи строк с префиксом длины
        f.writeRCString = async function(text) {
            await f.writeUInt16(text.length);
            await f.writeString(text);
        }

        // Заголовок
        await f.writeString(('RC3D'));                     // 4 байта    | "RC3D"
        await f.writeUInt16((3));                          // 2 байта    | Версия файла

        // Блок с материалами
        await f.writeUInt16(!stripMaterials ? model.materials.length : 0);
        if (model.materials.length && !stripMaterials) {
            console.log('Запись материалов...');
            for (const mat of model.materials) {

                console.log('Материал:', mat.name);

                // Флаги на наличие полей
                const paramFlags = [
                    !!mat.diffuse,      // Наличие diffuse-цвета
                    !!mat.ambient,      // Наличие ambient-цвета
                    !!mat.specular,     // Наличие specular-параметров
                    !!mat.emissive,     // Наличие emissive-цвета
                    !!mat.opacity,      // Наличие параметра прозрачности
                    false,
                    false,              // <- зарезервировано
                    false,
                ];
                await f.writeUInt8(this.composeFlag(paramFlags));

                if (paramFlags[0]) {
                    // Diffuse-цвет
                    await f.writeUInt8(mat.diffuse[0]);
                    await f.writeUInt8(mat.diffuse[1]);
                    await f.writeUInt8(mat.diffuse[2]);
                }
                if (paramFlags[1]) {
                    // Ambient-цвет
                    await f.writeUInt8(mat.ambient[0]);
                    await f.writeUInt8(mat.ambient[1]);
                    await f.writeUInt8(mat.ambient[2]);
                }
                if (paramFlags[2]) {
                    // Specular-цвет
                    await f.writeUInt8(mat.specular[0]);
                    await f.writeUInt8(mat.specular[1]);
                    await f.writeUInt8(mat.specular[2]);
                    await f.writeFloat(mat.specular_exponent);
                }
                if (paramFlags[3]) {
                    // Emissive-цвет
                    await f.writeUInt8(mat.emissive[0]);
                    await f.writeUInt8(mat.emissive[1]);
                    await f.writeUInt8(mat.emissive[2]);
                }
                if (paramFlags[4]) {
                    // Прозрачность
                    await f.writeUInt8(mat.opacity);
                }

                // Флаги наличия текстур
                const mapFlags = [
                    m(mat.diffuse_map),     // Diffuse
                    m(mat.specular_map),    // Specular
                    m(mat.emissive_map),    // Emissive
                    m(mat.normal_map),      // Normal
                    m(mat.bump_map),        // Bump
                    m(mat.alpha_map),       // Alpha
                    false,
                    false                   // <- зарезервировано
                ];
                await f.writeUInt8(this.composeFlag(mapFlags));

                // Запись названий текстур
                if (mapFlags[0]) await f.writeRCString(mat.diffuse_map);    // Diffuse
                if (mapFlags[1]) await f.writeRCString(mat.specular_map);   // Specular
                if (mapFlags[2]) await f.writeRCString(mat.emissive_map);   // Emissive
                if (mapFlags[3]) await f.writeRCString(mat.normal_map);     // Normal
                if (mapFlags[4]) await f.writeRCString(mat.bump_map);       // Bump
                if (mapFlags[5]) await f.writeRCString(mat.alpha_map);      // Alpha

                console.log('Флагов:', paramFlags.filter(g => g === true).length, ', текстур', mapFlags.filter(g => g === true).length)
            }
            console.log('');
        }

        // Настройки сжатия
        await f.writeUInt16(model.surfaces.length);        // 2 байта    | Количество подмоделей
        await f.writeUInt8(vertexCompress);
        await f.writeUInt8(normalCompress);
        await f.writeUInt8(colorCompress);

        // Запись сурфейсов (подмоделей)
        for (const data of model.surfaces) {

            // Предварительный перевод данных
            console.log('Сюрфейс: ', data.name);
            console.log('Вершин: '+data.verts.length+', индексов: '+data.indices.length);

            // Заголовок подмодели
            await f.writeRCString(data.name);
            await f.writeUInt16(                            // 2 байта    | Индекс материала (65535 - нет материала) (с версии 2)
                data.material !== false ?
                    data.material :
                    65535
            );

            // Запись вершин
            let hasPositions        = data.verts[0].position  !== false;
            let hasNormals          = data.verts[0].normal    !== false && !stripNormals;
            let hasColors           = data.verts[0].color     !== false && !stripColors;
            let hasTexCoords        = data.verts[0].texCoord  !== false && !stripTexCoords;
            let hasSecondTexCoords  = data.verts[0].texCoord2 !== false && !stripTexCoords;
            let hasSkinning         = false;
            await f.writeInt32(data.verts.length);                      // 4 байта    | Количество вершин
            await f.writeUInt8(hasPositions         ? 1 : 0);     // 1 байт     | Есть ли в модели вершины
            await f.writeUInt8(hasNormals           ? 1 : 0);     // 1 байт     | Есть ли в модели нормали
            await f.writeUInt8(hasColors            ? 1 : 0);     // 1 байт     | Есть ли в модели цвета
            await f.writeUInt8(hasTexCoords         ? 1 : 0);     // 1 байт     | Есть ли в модели текстурные координаты
            await f.writeUInt8(hasSecondTexCoords   ? 1 : 0);     // 1 байт     | Есть ли в модели вторые текстурные координаты
            await f.writeUInt8(hasSkinning          ? 1 : 0);     // 1 байт     | Есть ли в модели инфа по скиннингу [v2]
            await f.writeUInt8(0);
            await f.writeUInt8(0);                                // 2 байта    | Резерв флагов

            // Запись координат вершин
            if (hasPositions) {
                if (vertexCompress > 0) {
                    console.log('Вычисление bbox');

                    // Вычисление и сохранение bbox
                    const bbox = this.getBoundBox(data);
                    await f.writeFloat(bbox.origin[0]);
                    await f.writeFloat(bbox.origin[1]);
                    await f.writeFloat(bbox.origin[2]);
                    await f.writeFloat(bbox.size[0]);
                    await f.writeFloat(bbox.size[1]);
                    await f.writeFloat(bbox.size[2]);

                    // Перевод координат вершин
                    console.log('Запись сжатых координат');
                    for (let vertex of data.verts) {

                        // Для каждого из компонента координат
                        for (let idx = 0; idx < 3; idx++) {

                            let pos = (vertex.position[idx] - bbox.origin[idx]) / bbox.size[idx];
                            if (vertexCompress === 2) {

                                // Для сильного сжатия пишем в байт
                                await f.writeUInt8(Math.round(pos * 255.0));

                            } else {

                                // Для среднего сжатия - в шорт
                                await f.writeUInt16(Math.round(pos * 65535.0));

                            }

                        }
                    }

                } else {
                    console.log('Запись координат');

                    // Обычные флоуты с позицией вершины
                    for (let vertex of data.verts) {
                        await f.writeFloat(vertex.position[0]);
                        await f.writeFloat(vertex.position[1]);
                        await f.writeFloat(vertex.position[2]);
                    }

                }
            }

            // Запись нормалей
            if (hasNormals) {
                if (normalCompress > 0) {
                    console.log('Запись сжатых нормалей');

                    for (let vertex of data.verts) {

                        // Нормали вершины переводятся из вектора
                        // в широту-долготу и сохраняются в двух байтах

                        // Существуют два частных случая, когда нормаль направлена
                        // строго вверх или строго вниз - в этом случае у atan2
                        // получится NaN - пишем специальные значения
                        if (vertex.normal[1] === 1) {

                            // Для направления вверх - [0, 0]
                            await f.writeUInt8(0);
                            await f.writeUInt8(0);

                        } else if (vertex.normal[1] === -1) {

                            // Для направления вниз
                            await f.writeUInt8(0);
                            await f.writeUInt8(128);

                        } else {

                            // Конвертим нормаль в широту-долготу
                            const lng = (Math.atan2(vertex.normal[2], vertex.normal[0]) * 255 / (Math.PI * 2)) & 255;
                            const lat = (Math.acos(vertex.normal[1]) * 255 / (Math.PI * 2)) & 255;
                            await f.writeUInt8(lng);
                            await f.writeUInt8(lat);

                            /*
                                Декодинг:
                                lng <- readUInt8() * (2 * pi) / 255
                                lat <- readUInt8() * (2 * pi ) / 255
                                x <- cos ( lng ) * sin ( lat )
                                y <- cos ( lat )
                                z <- sin ( lng ) * sin ( lat )
                             */

                        }

                    }

                } else {
                    console.log('Запись нормалей');

                    // Обычные флоуты с вектором нормали
                    for (let vertex of data.verts) {
                        await f.writeFloat(vertex.normal[0]);
                        await f.writeFloat(vertex.normal[1]);
                        await f.writeFloat(vertex.normal[2]);
                    }

                }
            }

            // Запись цветов
            if (hasColors) {
                if (colorCompress > 0) {
                    console.log('Запись сжатых цветов');

                } else {
                    console.log('Запись цветов');

                    // Цвет вершины, сконверченный в байты
                    for (let vertex of data.verts) {
                        await f.writeUInt8(vertex.color[0] * 255.0);
                        await f.writeUInt8(vertex.color[1] * 255.0);
                        await f.writeUInt8(vertex.color[2] * 255.0);
                    }

                }
            }

            // Запись текстурных координат
            if (hasTexCoords) {
                console.log('Запись текстурных координат');

                // Координаты - обычные флоуты
                for (let vertex of data.verts) {
                    await f.writeFloat(vertex.texCoord[0]);
                    await f.writeFloat(vertex.texCoord[1]);
                }

            }

            // Запись индексов
            await f.writeInt32(data.indices.length);
            let bytesPerIndex = 4;
            if (data.verts.length <= 255) {
                bytesPerIndex = 1;
            } else if (data.verts.length <= 65535) {
                bytesPerIndex = 2;
            }
            await f.writeUInt8(bytesPerIndex);
            console.log('Запись индексов');
            console.log('Байт на индекс: '+bytesPerIndex);
            for (let index of data.indices) {
                switch (bytesPerIndex) {

                    // Запись в виде полного инта
                    case 4:
                        await f.writeInt32(index);
                        break;

                    // Запись в виде шорта
                    case 2:
                        await f.writeUInt16(index);
                        break;

                    // Запись в виде одного байта
                    case 1:
                        await f.writeUInt8(index);
                        break;

                    default:
                        console.log('Неизвестный размер вершины');
                        break;
                }
            }

            console.log('');
        }

        // Блок со скиннингом и костями
        await f.writeUInt16(0);         // Количество костей [v2]
        await f.writeFloat(0);          // Длина анимации [v2]

        // Закрываем файл
        await f.close();

    },

    /**
     * Полученение размеров модели
     * @param surf
     */
    getBoundBox(surf) {

        // Изначальные размеры
        let min = [
            Infinity, Infinity, Infinity
        ];
        let max = [
            -Infinity, -Infinity, -Infinity
        ];

        // Проходимся по вершинам и собираем размеры
        for (let vertex of surf.verts) {
            const p = vertex.position;
            for (let i = 0; i < 3; i++) {
                if (p[i] > max[i]) max[i] = p[i];
                if (p[i] < min[i]) min[i] = p[i];
            }
        }

        // Выдача
        max[0] -= min[0];
        max[1] -= min[1];
        max[2] -= min[2];
        return {
            origin: min,
            size: max
        }
    },

    /**
     * Сборка флагов в число
     * @param flags Массив флагов
     * @returns {number}
     */
    composeFlag(flags) {
        let out = 0;
        for (let i = 0; i < flags.length; i++) {
            if (flags[i]) {
                out += 1 << i;
            }
        }
        return out;
    }
}