
import * as fs from 'fs/promises';

function humanFileSize(size) {
    if (size === 0) {
        return 0;
    }
    const i = Math.floor( Math.log(size) / Math.log(1024) );
    return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

/**
 * Описание статуса экспорта
 * @param in_file
 * @param out_file
 * @param time_import
 * @param time_export
 */
export default async function (in_file, out_file, time_import, time_export) {

    // Размеры файлов
    const inStat = await fs.stat(in_file);
    const outStat = await fs.stat(out_file);

    // Профит
    let profit = 100 - outStat.size / inStat.size * 100;

    // Вывод
    console.log('='.repeat(80));
    console.log('Успешно! Профит по сжатию:', Math.round(profit)+'%');
    console.log('Исходный файл: размер', humanFileSize(inStat.size), ', парсинг:', time_import, 'мс.');
    console.log('Конечный файл: размер', humanFileSize(outStat.size), ', обработка:', time_export, 'мс.');

}